package Bank;

import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.text.html.HTMLDocument.Iterator;

public abstract class Account 
{
	private int acountnumber;
	protected float deposit;
	protected ArrayList<String> changelog;
	public Account(float startdeposit,int nouacountnumber)
	{
		this.deposit=startdeposit;
		this.acountnumber=nouacountnumber;
		changelog=new ArrayList<String>();
	}
	protected Account() {}
	public int getaccountnumber()
	{
		return this.acountnumber;
	}
	public float getdeposit()
	{
		return this.deposit;
	}
  public abstract boolean depositMoney(int money);
  public abstract int withdraw(int money);
  public ArrayList<String> getLog()
  {
	  return this.changelog;
  }
}
