package Bank;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.html.HTMLDocument.Iterator;

public class Bank 
{
  private HashSet<Account> bankAcounts;
  private HashMap<Integer,ArrayList<Person>> accountholders;
  private ArrayList<Person> clients;
  public Bank()
  {
	  bankAcounts=new HashSet<Account>();
	  accountholders=new HashMap<Integer,ArrayList<Person>>();
	  clients=new ArrayList<Person>();
  }
  public boolean well_formed(Class type,Object ob1,Object ob2,Object ob3,Object ob4)
  {
	  boolean rezultat=true;
	  
	switch(type.getSimpleName())
	{
	case "Person":
	 try
	 {
		 int i;
		 String.valueOf(ob1);
		 i=(int) Integer.valueOf((String) ob2);
		 i=(int) Integer.valueOf((String) ob3);
		 String.valueOf(ob4);
	 }
	 catch(Exception e)
	 {
		 rezultat=false;
	 }
	 break;
	case "SavingAccount":
		 try
		 {
			 int i;
			 float f;
			 i=(int) Integer.valueOf((String) ob1);
			 f=(float) Float.valueOf((String) ob2);
			 f=(float) Float.valueOf((String) ob3);
		 }
		 catch(Exception e)
		 {
			 rezultat=false;
		 }
		 break;
  case "SpendingAccount":
		 try
		 {
			 int i;
			 float f;
			 i=(int) Integer.valueOf((String) ob1);
			 f=(float) Float.valueOf((String) ob2);
		 }
		 catch(Exception e)
		 {
			 rezultat=false;
		 }
		 break;
	}
	return rezultat;
  }
  public boolean createPerson(String nume,String cnp,String varsta,String locatie)
  {
	  if(well_formed(Person.reflectie.getClass(),nume,cnp,varsta,locatie))
	  {
		  boolean contine=false;
		  for(Person a:clients)
		  {
			  if(a.getNume().equals(nume)) contine=true;
		  }
		  if(!contine)
		  {
		   Person temp=new Person(nume,Integer.parseInt(cnp),Integer.parseInt(varsta),locatie);
		   clients.add(temp);
		   return true;
		  }
	  }
	  return false;
  }
  public boolean createSavingAccount(String numar,String startdeposit,String interest)
  {
	  if(well_formed(SavingAccount.reflectie.getClass(),numar,startdeposit,interest,null))
	  {
		  if(!accountholders.containsKey(numar))
		  {
		   SavingAccount temp=new SavingAccount(Integer.parseInt(numar),Float.parseFloat(startdeposit),Float.parseFloat(interest));
		   accountholders.put(Integer.parseInt(numar),new ArrayList<Person>());
		   bankAcounts.add(temp);
		   return true;
		  }
	  }
	  return false;
  }
  public boolean createSpendingAccount(String numar,String startdeposit)
  {
	  if(well_formed(SpendingAccount.reflectie.getClass(),numar,startdeposit,null,null))
	  {
		  if(!accountholders.containsKey(numar))
		  {
		   SpendingAccount temp=new SpendingAccount(Integer.parseInt(numar),Float.parseFloat(startdeposit));
		   accountholders.put(Integer.parseInt(numar),new ArrayList<Person>());
		   bankAcounts.add(new SpendingAccount(Integer.parseInt(numar),Float.parseFloat(startdeposit)));
		   return true;
		  }
	  }
	  return false;
  }
  public boolean removeAccount(int numar)
  {
	  boolean rezultat=false;
	  assert(accountholders.containsKey(numar));
	  accountholders.remove(numar);
	 
	  java.util.Iterator<Account> it=bankAcounts.iterator();
		  Account temp;
		  while(it.hasNext())
		  {
			  temp=it.next();
			  if(temp.getaccountnumber()==numar) 
				  {
				    bankAcounts.remove(temp);
				    rezultat=true;
				  }
		  }
	  
	 
	  return rezultat;
  }
  public boolean removePerson(String nume)
  {
	  Person temp=null;
	  for(Person temp1:clients)
	  {
		  if(temp1.getNume().equals(nume)) 
		  {
			  temp=temp1;
		  }
	  }
	  assert(temp!=null);
	  clients.remove(temp);
	  return true;
  }
  public String geteditPerson(String nume,int option)
  {
	  String rezultat=null;
	  Person selectedPerson=null;
	  assert(nume!=null);
	  for(Person a:clients)
	  {
		  if(a.getNume().equals(nume)) selectedPerson=a;
	  }
	  switch(option)
	  {
	  case 0:
	  rezultat=Integer.toString(selectedPerson.getCNP());
	  break;
	  case 1:
		  rezultat=Integer.toString(selectedPerson.getVarsta());
	  break;
	  case 2:
	  rezultat=selectedPerson.getLocatie();
	  break;
	  }
	  return rezultat;
  }
  public boolean editPerson(String nume,int option,String newValue)
  {
	  boolean rezultat=true;
	  Person selectedPerson=null;
	  assert(nume!=null);
	  for(Person a:clients)
	  {
		  if(a.getNume().equals(nume)) selectedPerson=a;
	  }
	  assert(selectedPerson!=null);
	  try 
	  {
	  switch(option)
	  {
	  case 0:
	  selectedPerson.setCNP(Integer.parseInt(newValue));
	  break;
	  case 1:
	  selectedPerson.setVarsta(Integer.parseInt(newValue));
	  break;
	  case 2:
	  selectedPerson.setLocatie(newValue);
	  break;
	  }
	  }
	  catch(Exception e)
	  {
		  rezultat=false;
	  }
	  return rezultat;
  }
  public boolean money(int option,String numberofAccount,String nume,String money)
  {
	  boolean rezultat=false,gasit=false;
	  Account selectat=null;
	  if(accountholders.containsKey(Integer.parseInt(numberofAccount))&&Integer.parseInt(money)>0)
	  {
		  
		  for(Person per:accountholders.get(Integer.parseInt(numberofAccount)))
		  {
			  if(per.getNume().equals(nume))
			  {
				  gasit=true;
			  }
		  }
		  if(gasit)
		  {
			  java.util.Iterator<Account> it=bankAcounts.iterator();
			  Account temp=null;
			  while(it.hasNext())
			  {
				  temp=it.next();
				  if(temp.getaccountnumber()==Integer.parseInt(numberofAccount)) 
					  {
					    selectat=temp;
					  }
			  }
			  assert(selectat!=null);
			  switch(option)
			  {
			  case 0://deposit
			  if(selectat.depositMoney(Integer.parseInt(money))) rezultat=true;
			  break;
			  case 1://withdraw
			  if(selectat.withdraw(Integer.parseInt(money))!=-1) rezultat=true;
			  break;
			  }
		  }
	  }
	  return rezultat;
  }
  public JTable viewPersons()
  {
	  int nr=0;
	  JTable rezultat;
	  DefaultTableModel model=new DefaultTableModel();
	  for(Field a:Person.reflectie.getClass().getDeclaredFields())
	  {
		  if(!a.getName().equals("reflectie"))
		  {
		  model.addColumn(a.getName());
		  nr++;
		  }
	  }
	  assert(nr!=4);
	  Object[] ob=new Object[nr];
	  nr=0;
	  for(Field a:Person.reflectie.getClass().getDeclaredFields())
	  {
		  if(!a.getName().equals("reflectie"))
		  {
		  ob[nr]=a.getName();
		  nr++;
		  }
	  }
	  model.addRow(ob);
	  for(Person a:clients)
	  {
		  ob[0]=a.getNume();
		  ob[1]=a.getCNP();
		  ob[2]=a.getVarsta();
		  ob[3]=a.getLocatie();
		  model.addRow(ob);
	  }
	  rezultat=new JTable(model);
	  return rezultat;
	         
  }
  public JTable viewAccounts()
  {
	  int nr=0;
	  JTable rezultat;
	  DefaultTableModel model=new DefaultTableModel();
	  model.addColumn("AccountNumber");
	  model.addColumn("Accountdeposit");
	  model.addColumn("AccountType");
	  Object[] ob=new Object[3];
	  ob[0]="AccountNumber";
	  ob[1]="Accountdeposit";
	  ob[2]="AccountType";
	  model.addRow(ob);
	  java.util.Iterator<Account> it=bankAcounts.iterator();
	  Account temp;
	  while(it.hasNext())
	  {
		  temp=it.next();
		  ob[0]=temp.getaccountnumber();
		  ob[1]=temp.getdeposit();
		  ob[2]=temp.getClass().getSimpleName();
		  model.addRow(ob);
	  }
	  rezultat=new JTable(model);
	  return rezultat;
	         
  }
  public String[] viewAcc()
  {
	  String[] rezultat=null;
	  java.util.Iterator<Account> it=bankAcounts.iterator();
	  int nr=0;
	  while(it.hasNext())
	  {
		 it.next();
		 nr++;  
	  }
	  rezultat=new String[nr];
	  it=bankAcounts.iterator();
	  nr=0;
	  while(it.hasNext())
	  {
		  rezultat[nr]=Integer.toString(it.next().getaccountnumber());  
		  nr++;
	  }
	  return rezultat;
  }
  public String[] viewPer()
  {
	  String[] rezultat=null;
	  int nr=0;
	  for(Person a:clients)
	  {
		  nr++;
	  }
	  rezultat=new String[nr];
	  nr=0;
	  for(Person a:clients)
	  {
		  rezultat[nr]=a.getNume();
		  nr++;
	  }
	  return rezultat;
  }
  public boolean giveas(String nr,String nume)
  {
	  boolean rezultat=false,contine=false;
	  Person cli=null;
	  int nr1=Integer.parseInt(nr);
	  for(Person a:accountholders.get(nr1))
	  {
		  if(a.getNume().equals(nume)) contine=true;
	  }
	  if(!contine)
	  {
		  for(Person a:clients)
		  {
			  if(a.getNume().equals(nume)) cli=a;
		  }
		  assert(cli!=null);
		  accountholders.get(nr1).add(cli);
		  rezultat=true;
	  }
	  return rezultat;
  }
  public boolean removeas(String nr,String nume)
  {
	  boolean rezultat=false,contine=false;
	  int nr1=Integer.parseInt(nr);
	  Person cli=null;
	  for(Person a:accountholders.get(nr1))
	  {
		  if(a.getNume().equals(nume)) contine=true;
	  }
	  if(contine)
	  {
		  for(Person a:clients)
		  {
			  if(a.getNume().equals(nume)) cli=a;
		  }
		  assert(cli!=null);
		  accountholders.get(nr1).remove(cli);
		  rezultat=true;
	  }
	  return rezultat;
  }
  public JTable viewacces(int accountnumber)
  {
	 
	  JTable rezultat=null;int nr=0;
	  DefaultTableModel model=new DefaultTableModel();
	  for(Field a:Person.reflectie.getClass().getDeclaredFields())
	  {
		  if(!a.getName().equals("reflectie"))
		  {
		  model.addColumn(a.getName());
		  nr++;
		  }
	  }
	  assert(nr!=4);
	  Object[] ob=new Object[nr];
	  nr=0;
	  for(Field a:Person.reflectie.getClass().getDeclaredFields())
	  {
		  if(!a.getName().equals("reflectie"))
		  {
		   ob[nr]=a.getName();
		  nr++;
		  }
	  }
	  model.addRow(ob);
	  nr=0;
	  for(Person per:accountholders.get(accountnumber))
	  {
		  ob[0]=per.getNume();
		  ob[1]=per.getCNP();
		  ob[2]=per.getVarsta();
		  ob[3]=per.getLocatie();
		  model.addRow(ob);
	  }
	  rezultat=new JTable(model);
	  return rezultat;
  }
  public JTextArea getLog(int numaraccount,JPanel wrapper)
  {
	  JTextArea rezultat=new JTextArea();
	  java.util.Iterator<Account> it=bankAcounts.iterator();
	  ArrayList<String> tem=null;
	  Account temp;
	  while(it.hasNext())
	  {
		  temp=it.next();
		  if(temp.getaccountnumber()==numaraccount) 
			  {
			   tem=temp.getLog();
			  }
	  }
	  for(String str:tem)
	  {
		  rezultat.setText(rezultat.getText() +str);
	  }
	  return rezultat;
  }
}
