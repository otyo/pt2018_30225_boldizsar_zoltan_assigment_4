package Bank;

public class Person 
{
  private  String nume;
  private int cnp;
  private int varsta;
  private String locatie;
  private Person(){}
  
  public Person(String nume_in,int cnp_in,int varsta_in,String locatie_in)
  {
	  this.nume=nume_in;
	  this.cnp=cnp_in;
	  this.varsta=varsta_in;
	  this.locatie=locatie_in;
  }
  public static Person reflectie=new Person();
  public String getNume()
  {
	  return this.nume;
  }
  public int getCNP()
  {
	  return this.cnp;
  }
  public int getVarsta()
  {
	  return this.varsta;
  }
  public String getLocatie()
  {
	  return this.locatie;
  }
  public void setNume(String nou)
  {
	  this.nume=nou;
  }
  public void setCNP(int nou)
  {
	  this.cnp=nou;
  }
  public void setVarsta(int nou)
  {
	  this.varsta=nou;
  }
  public void setLocatie(String nou)
  {
	  this.locatie=nou;
  }
}

