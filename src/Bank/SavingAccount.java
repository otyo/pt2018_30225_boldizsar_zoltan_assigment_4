package Bank;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

public class SavingAccount extends Account
{
	int nr_claims,nr_deposits;
	long lastlogoutdate;
	float interest;
	public static SavingAccount reflectie=new SavingAccount();
	private SavingAccount() {}
    public SavingAccount(int accountnumber,float startdeposit,float nouinterest) 
    {
		super(startdeposit,accountnumber);
		this.nr_claims=0;
		this.nr_deposits=0; 
		this.interest=nouinterest;
		lastlogoutdate=0;
	}
	public boolean depositMoney(int money) 
	{
		this.login();
		if(nr_deposits>0) return false;
		this.changelog.add("Sa depozitat "+money+" bani");
		nr_deposits++;
		this.deposit+=money;
		return true;
	}
	public int withdraw(int money) 
	{
		this.login();
		if(nr_claims>0) return -1;
		if(money>deposit) return -1;
		this.changelog.add("Sa scos "+money+" bani");
		nr_claims++;
		this.deposit-=money;
		return money;
	}	
	private void login()
	{
		this.deposit+=deposit*interest*(Calendar.getInstance().get(Calendar.DAY_OF_YEAR)-lastlogoutdate);
		lastlogoutdate=Calendar.getInstance().get(Calendar.DAY_OF_YEAR)-lastlogoutdate;
	}
	private void logout()
	{
		this.nr_claims=0;
		this.nr_deposits=0;
	}
}
