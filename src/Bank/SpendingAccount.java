package Bank;

public class SpendingAccount extends Account
{

	public SpendingAccount(int accountnumber,float startdeposit) 
	{
		super(startdeposit,accountnumber);
	}
	public static SpendingAccount reflectie=new SpendingAccount();
	private SpendingAccount() {}
	public boolean depositMoney(int money) 
	{
		this.changelog.add("Sa depozitat "+money+" bani");
		this.deposit+=money;
		return true;
	}
	public int withdraw(int money) 
	{
		if(deposit>=money)
		{
			this.changelog.add("Sa scos "+money+" bani");
			deposit-=money;
			return money;
		}
		else
		{
			return -1;
		}
	}

}
