package Interface;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import Bank.Bank;

public class BankProc extends JFrame implements ActionListener,MouseListener

{
	private JTable temp4;
	private Bank displayBank;
	private JMenuBar mymenu;
	private JMenu person,account,view,editaccount,useBank;
	private JPanel panel,panel1;
	private JTextField text1,text2,text3,text4;
	private JLabel label1,label2,label3,label4,label5;
	private JComboBox box1,box2;
	private JButton commit;
	private JMenuItem addperson,removeperson,editperson,addaccount,removeaccount,viewaccount,viewperson,addacces,removeacces,withdraw,deposit,viewlog;
	public BankProc()
	{
		viewlog=new JMenuItem("ViewLog");
		viewlog.addActionListener(this);
		displayBank=new Bank();
		useBank=new JMenu("Use Bank");
		withdraw=new JMenuItem("Withdraw");
		withdraw.addActionListener(this);
		deposit=new JMenuItem("Deposit");
		deposit.addActionListener(this);
		useBank.add(withdraw);
		useBank.add(deposit);
	  	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	  	double width = screenSize.getWidth();
	  	double height = screenSize.getHeight();
	  	this.setTitle("Bank");
	  	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  	if(width>600&&height>400)
	  	{
	  		this.setSize(600,400);
	  		this.setLocation((int) width/2-300,(int) height/2-200);
	  	}
	  	else
	  	{
	  		this.setSize((int) width,(int) height);
	  	}
	  	panel=new JPanel();
	  	mymenu=new JMenuBar();
	  	person=new JMenu("Person");
	  	account=new JMenu("Account");
	  	view=new JMenu("View");
	  	addperson=new JMenuItem("Add Person");
	  	editperson=new JMenuItem("Edit Person");
	  	removeperson=new JMenuItem("Remove Person"); 
	  	addaccount=new JMenuItem("Add Account");
	  	editaccount=new JMenu("Edit Account");
	  	removeaccount=new JMenuItem("Remove Account");
	  	viewperson=new JMenuItem("View Persons");
	  	viewaccount=new JMenuItem("View Accounts");
	  	addacces=new JMenuItem("Add Acces");
	  	removeacces=new JMenuItem("Remove Acces");
	  	addperson.addActionListener(this);
	  	editperson.addActionListener(this);
	  	removeperson.addActionListener(this);
	  	viewperson.addActionListener(this);
	  	addaccount.addActionListener(this);
	  	addacces.addActionListener(this);
	  	removeacces.addActionListener(this);
	  	removeaccount.addActionListener(this);
	  	viewaccount.addActionListener(this);
	  	editaccount.add(addacces);
	  	editaccount.add(removeacces);
	  	person.add(addperson);
	  	person.add(editperson);
	  	person.add(removeperson);
	  	account.add(addaccount);
	  	account.add(editaccount);
	  	account.add(removeaccount);
	  	account.add(useBank);
	  	account.add(viewlog);
	  	view.add(viewaccount);
	  	view.add(viewperson);
	  	mymenu.add(person);
	  	mymenu.add(account);
	  	mymenu.add(view);
	  	this.add(panel);
	  	this.setJMenuBar(mymenu);
	  	this.setVisible(true);
	}
 public static void main(String[] args)
 {
	 BankProc interfata=new BankProc();
 }
@Override
public void actionPerformed(ActionEvent apasat) 
{
	String comanda=apasat.getActionCommand();
	switch(comanda)
	{
	case "Add Person":
		panel.removeAll();
		panel1=new JPanel();
		panel1.setLayout(new GridLayout(0,2));
		label1=new JLabel("Nume:");
		panel1.add(label1);
		text1=new JTextField(20);
		panel1.add(text1);
		label2=new JLabel("CNP:");
		panel1.add(label2);
		text2=new JTextField(20);
		panel1.add(text2);
		label3=new JLabel("Varsta:");
		panel1.add(label3);
		text3=new JTextField(20);
		panel1.add(text3);
		label4=new JLabel("Locatie:");
		panel1.add(label4);
		text4=new JTextField(20);
		panel1.add(text4);
		label5=new JLabel("Add:");
		panel1.add(label5);
		commit=new JButton("AddPerson");
		commit.addActionListener(this);
		panel1.add(commit);
		panel.add(panel1);
	    this.revalidate();
	    this.repaint();
		break;
   case "Edit Person":
	   String[] temp1={"CNP","Varsta","Locatie"};
		  panel.removeAll();
			panel1=new JPanel();
			panel1.setLayout(new GridLayout(0,2));
			label1=new JLabel("Account to modify:");
			panel1.add(label1);
			box1=new JComboBox(displayBank.viewPer());
			panel1.add(box1);
			label2=new JLabel("Field to modify");
			panel1.add(label2);
			box2=new JComboBox(temp1);
			panel1.add(box2);
			label5=new JLabel("Select:");
			panel1.add(label5);
			commit=new JButton("Select Account and field");
			panel1.add(commit);
			commit.addActionListener(this);
			panel.add(panel1);
		    this.revalidate();
		    this.repaint();
			break;
   case "Remove Person":
	   panel.removeAll();
		  panel1=new JPanel();
		  panel1.setLayout(new GridLayout(0,2));
		  label1=new JLabel("Person to remove:");
		  panel1.add(label1);
		  box1=new JComboBox(displayBank.viewPer());
		  panel1.add(box1);
		  label5=new JLabel("Remove:");
			panel1.add(label5);
			commit=new JButton("RemovePerson");
			panel1.add(commit);
			commit.addActionListener(this);
			panel.add(panel1);
		    this.revalidate();
		    this.repaint();
			break;
   case "View Persons":
	   panel.removeAll();
		  panel.add(displayBank.viewPersons());
		  this.revalidate();
		  this.repaint();
		break;
   case "Add Account":
	   String[] temp={"SavingAccount","SpendingAccount"};
	   panel.removeAll();
		panel1=new JPanel();
		panel1.setLayout(new GridLayout(0,2));
		label1=new JLabel("AccountNumber:");
		panel1.add(label1);
		text1=new JTextField(20);
		panel1.add(text1);
		label2=new JLabel("Startdeposit");
		panel1.add(label2);
		text2=new JTextField(20);
		panel1.add(text2);
		label3=new JLabel("interestrate:(optional)");
		panel1.add(label3);
		text3=new JTextField(20);
		panel1.add(text3);
		label4=new JLabel("Tip:");
		panel1.add(label4);
		box1=new JComboBox(temp);
		panel1.add(box1);
		label5=new JLabel("Add:");
		panel1.add(label5);
		commit=new JButton("AddAccount");
		commit.addActionListener(this);
		panel1.add(commit);
		panel.add(panel1);
	    this.revalidate();
	    this.repaint();
		break;
  case "Add Acces":
	  panel.removeAll();
	  panel1=new JPanel();
	  panel1.setLayout(new GridLayout(0,2));
	  label1=new JLabel("Account:");
	  panel1.add(label1);
	  box1=new JComboBox(displayBank.viewAcc());
	  panel1.add(box1);
	  label2=new JLabel("Person:");
	  panel1.add(label2);
	  box2=new JComboBox(displayBank.viewPer());
	  panel1.add(box2);
	  panel.add(panel1);
	  label3=new JLabel("Add:");
	  panel1.add(label3);
	  commit=new JButton("Give Acces");
	  commit.addActionListener(this);
	  panel1.add(commit);
	  panel.add(panel1);
	  this.revalidate();
	  this.repaint();
		break;
  case "Remove Acces":
	  panel.removeAll();
	  panel1=new JPanel();
	  panel1.setLayout(new GridLayout(0,2));
	  label1=new JLabel("Account:");
	  panel1.add(label1);
	  box1=new JComboBox(displayBank.viewAcc());
	  panel1.add(box1);
	  label2=new JLabel("Persons:");
	  panel1.add(label2);
	  box2=new JComboBox(displayBank.viewPer());
	  panel1.add(box2);
	  panel.add(panel1);
	  label3=new JLabel("Remove:");
	  panel1.add(label3);
	  commit=new JButton("RemoveAcces");
	  commit.addActionListener(this);
	  panel1.add(commit);
	  commit.addActionListener(this);
	  panel.add(panel1);
	  this.revalidate();
	  this.repaint();
		break;
  case "Remove Account":
	  panel.removeAll();
	  panel1=new JPanel();
	  panel1.setLayout(new GridLayout(0,2));
	  label1=new JLabel("Account to remove:");
	  panel1.add(label1);
	  box1=new JComboBox(displayBank.viewAcc());
	  panel1.add(box1);
	  label5=new JLabel("Remove:");
		panel1.add(label5);
		commit=new JButton("RemoveAccount");
		commit.addActionListener(this);
		panel1.add(commit);
		panel.add(panel1);
	    this.revalidate();
	    this.repaint();
		break;
  case "View Accounts":
	  panel.removeAll();
	  temp4=displayBank.viewAccounts();
	  panel.add(temp4);
	  temp4.addMouseListener(this);
	  this.revalidate();
	  this.repaint();
		break;
	 case "AddPerson":
		 if(displayBank.createPerson(text1.getText(),text2.getText(),text3.getText(),text4.getText()))
		 {
			 JOptionPane.showMessageDialog(null,"Create Person Succes!");
		 }
		 else
		 {
			 JOptionPane.showMessageDialog(null,"Create Person Fail");
		 }
			break;
	 case "AddAccount":
	   switch(box1.getSelectedIndex())
	   {
	    case 0:
	    	if(displayBank.createSavingAccount(text1.getText(),text2.getText(),text3.getText()))
	    	 {
				 JOptionPane.showMessageDialog(null,"Create SavingsAccount Succes!");
			 }
			 else
			 {
				 JOptionPane.showMessageDialog(null,"Create SavingsAccount Fail");
			 }
	    break;
	    case 1:
	    	if(displayBank.createSpendingAccount(text1.getText(),text2.getText()))
	    	 {
				 JOptionPane.showMessageDialog(null,"Create SpendingAccount Succes!");
			 }
			 else
			 {
				 JOptionPane.showMessageDialog(null,"Create SpendingAccount Fail");
			 }
	    break;
	   }
			break;
	 case "RemovePerson":
		 if(displayBank.removePerson((String) box1.getSelectedItem()))
		 {
			 JOptionPane.showMessageDialog(null,"Remove Person Succes!");
		 }
		 else
		 {
			 JOptionPane.showMessageDialog(null,"Remove Person Fail");
		 }
			break;
	 case "RemoveAccount":
		 if(displayBank.removeAccount(Integer.parseInt((String) box1.getSelectedItem())))
		 {
			 JOptionPane.showMessageDialog(null,"Remove Account Succes!");
		 }
		 else
		 {
			 JOptionPane.showMessageDialog(null,"Remove Account Fail");
		 }
			break;
	 case "Select Account and field":
		 panel.removeAll();
		  panel1=new JPanel();
		  panel1.setLayout(new GridLayout(0,2));
		  label1=new JLabel("Field to modify:");
		  panel1.add(label1);
		  text1=new JTextField(displayBank.geteditPerson((String) box1.getSelectedItem(),box2.getSelectedIndex()));
		  panel1.add(text1);
		  label2=new JLabel("Edit:");
		  panel1.add(label2);
			commit=new JButton("Edit Selected Field");
			commit.addActionListener(this);
			panel1.add(commit);
			commit.addActionListener(this);
			panel.add(panel1);
		    this.revalidate();
		    this.repaint();
		 break;
	   case "Edit Selected Field":
		   if(displayBank.editPerson((String) box1.getSelectedItem(),box2.getSelectedIndex(),text1.getText()))
		   {
				 JOptionPane.showMessageDialog(null,"Edit Person Succes!");
			 }
			 else
			 {
				 JOptionPane.showMessageDialog(null,"Edit Person Fail");
			 }
		 break;
	   case "Give Acces":
		   if(displayBank.giveas((String) box1.getSelectedItem(),(String) box2.getSelectedItem()))
		   {
				 JOptionPane.showMessageDialog(null,"Add Acces Succes!");
			 }
			 else
			 {
				 JOptionPane.showMessageDialog(null,"Add Acces Fail");
			 }
		   break;
	   case "RemoveAcces":
		   if(displayBank.removeas((String) box1.getSelectedItem(),(String) box2.getSelectedItem()))
		   {
				 JOptionPane.showMessageDialog(null,"Remove Acces Succes!");
			 }
			 else
			 {
				 JOptionPane.showMessageDialog(null,"Remove Acces Fail");
			 }
		   break;
	   case "Withdraw":
		   panel.removeAll();
			  panel1=new JPanel();
			  panel1.setLayout(new GridLayout(0,2));
			  label1=new JLabel("Account:");
			  panel1.add(label1);
			  box1=new JComboBox(displayBank.viewAcc());
			  panel1.add(box1);
			  label2=new JLabel("Person:");
			  panel1.add(label2);
			  box2=new JComboBox(displayBank.viewPer());
			  panel1.add(box2);
			  label3=new JLabel("Amount:");
			  panel1.add(label3);
			  text1=new JTextField(20);
			  panel1.add(text1);
			  label3=new JLabel("Withdraw:");
			  panel1.add(label3);
			  commit=new JButton("Withdraw Money");
			  commit.addActionListener(this);
			  panel1.add(commit);
			  commit.addActionListener(this);
			  panel.add(panel1);
			  this.revalidate();
			  this.repaint();
		   break;
	   case "Deposit":
		   panel.removeAll();
			  panel1=new JPanel();
			  panel1.setLayout(new GridLayout(0,2));
			  label1=new JLabel("Account:");
			  panel1.add(label1);
			  box1=new JComboBox(displayBank.viewAcc());
			  panel1.add(box1);
			  label2=new JLabel("Person:");
			  panel1.add(label2);
			  box2=new JComboBox(displayBank.viewPer());
			  panel1.add(box2);
			  label3=new JLabel("Amount:");
			  panel1.add(label3);
			  text1=new JTextField(20);
			  panel1.add(text1);
			  label3=new JLabel("Deposit:");
			  panel1.add(label3);
			  commit=new JButton("Deposit Money");
			  commit.addActionListener(this);
			  panel1.add(commit);
			  commit.addActionListener(this);
			  panel.add(panel1);
			  this.revalidate();
			  this.repaint();
		   break;
	   case "Deposit Money":
		   if(displayBank. money(0,(String) box1.getSelectedItem(),(String) box2.getSelectedItem(),(String) text1.getText()))
		   {
				 JOptionPane.showMessageDialog(null,"Deposit Succes!");
			 }
			 else
			 {
				 JOptionPane.showMessageDialog(null,"Deposit Fail");
			 }
		   break;
	   case "Withdraw Money":
		   if(displayBank. money(1,(String) box1.getSelectedItem(),(String) box2.getSelectedItem(),(String) text1.getText()))
		   {
				 JOptionPane.showMessageDialog(null,"Withdraw Succes!");
			 }
			 else
			 {
				 JOptionPane.showMessageDialog(null,"Withdraw Fail");
			 }
		  break;
	   case "ViewLog":
		   panel.removeAll();
		   panel1=new JPanel();
		   panel1.setLayout(new GridLayout(0,2));
		   label1=new JLabel("Account:");
		   panel1.add(label1);
		   box1=new JComboBox(displayBank.viewAcc());
		   panel1.add(box1);
		   label2=new JLabel("View:");
		   panel1.add(label2);
		   commit=new JButton("View");
		   commit.addActionListener(this);
		   panel1.add(commit);
		   panel.add(panel1);
		   this.revalidate();
		   this.repaint();
		   break;
	    case "View":
	      panel.removeAll();
	      panel1=new JPanel();
	      JTextArea A=displayBank.getLog(Integer.parseInt((String) box1.getSelectedItem()),panel1);
	      panel1.add(A);
	      panel.add(panel1);
	      this.revalidate();
		  this.repaint();
		  break;
		 default:
			 JOptionPane.showMessageDialog(null,"Comanda neimplementata!");
			break;
			
	}
	
}

public void mouseClicked(MouseEvent arg0) {
	int i=temp4.getSelectedRow();
	
	if(i>0)
	{
		panel.removeAll();
		System.out.println(temp4.getValueAt(i,0));
		panel.add(displayBank.viewacces((int) temp4.getValueAt(i,0)));
		this.revalidate();
		this.repaint();
	}
}
@Override
public void mouseEntered(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}
@Override
public void mouseExited(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}
@Override
public void mousePressed(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}
@Override
public void mouseReleased(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}
}
